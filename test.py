# импортируем функцию find_shortest_path из модуля main
from main import find_shortest_path

# определяем функцию тестирования корректности кратчайшего пути
def test_correct_shortest_path():
    # задаем граф в виде словаря смежности
    graph = {
        'A': ['B', 'E', 'C'],
        'B': ['A', 'D', 'E'],
        'C': ['A', 'F', 'G'],
        'D': ['B', 'E'],
        'E': ['A', 'B', 'D'],
        'F': ['C', 'I'],
        'G': ['C', 'H'],
        'H': ['G', 'I', 'J'],
        'I': ['H', 'J', 'F'],
        'J': ['H', 'I']
    }
    # проверяем, что кратчайший путь из 'A' в 'J' равен (['A', 'C', 'F', 'I', 'J'], 110)
    assert find_shortest_path(graph, 'A', 'J') == (['A', 'C', 'F', 'I', 'J'], 110)

# вызываем функцию тестирования
test_correct_shortest_path()
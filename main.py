from collections import defaultdict

# функция для построения графа
def build_graph(edges):
    # используем defaultdict с типом list для автоматического создания пустых списков
    graph = defaultdict(list)

    # проходимся по каждому ребру и добавляем связь между узлами
    for edge in edges:
        a, b = edge[0], edge[1]

        graph[a].append(b)
        graph[b].append(a)

    # возвращаем граф
    return graph


# функция для поиска кратчайшего пути между двумя узлами в графе
def find_shortest_path(graph, start, goal):
    # переменная для подсчета количества операций
    operations_count = 0

    # список для отслеживания пройденных узлов
    explored = []

    # очередь для хранения путей
    queue = [[start]]

    # если начальный и конечный узлы совпадают
    if start == goal:
        # увеличиваем количество операций на 1 и возвращаем None вместо пути
        operations_count += 1
        return None, operations_count

    # пока очередь не пуста
    while queue:
        # извлекаем первый путь из очереди
        path = queue.pop(0)

        # получаем последний узел в пути
        node = path[-1]
        operations_count += 2

        # если узел еще не был пройден
        if node not in explored:
            # получаем соседей узла
            neighbours = graph[node]
            operations_count += 1

            # для каждого соседа
            for neighbour in neighbours:
                # создаем новый путь, добавляя соседа в конец
                new_path = list(path)
                new_path.append(neighbour)

                # добавляем новый путь в очередь
                queue.append(new_path)
                operations_count += 3

                # если сосед - целевая вершина
                if neighbour == goal:
                    # увеличиваем количество операций на 1 и возвращаем путь и количество операций
                    operations_count += 1
                    return new_path, operations_count

            # помечаем узел как пройденный
            explored.append(node)
            operations_count += 1

    # если путь не найден, возвращаем None и количество операций
    return None, operations_count

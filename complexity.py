from main import find_shortest_path
from itertools import combinations
import random
import networkx as nx
import matplotlib.pyplot as plt

# Функция для генерации случайного графа с определенным количеством вершин и вероятностью ребра
def generate_random_graph(number_of_dots, probability_of_edge):
    V = set([v for v in range(number_of_dots)])
    E = set()
    for combination in combinations(V, 2):
        a = random.random()
        if a < probability_of_edge:
            E.add(combination)

    graph = nx.Graph()
    graph.add_nodes_from(V)
    graph.add_edges_from(E)

    return graph

# Список количества операций в зависимости от размерности графа
operations_for_sizes = []

# Размеры графа, для которых будут производиться замеры сложности
sizes = [n for n in range(10, 110, 10)]

# Итерация по размерам графов
for i in range(10, 110, 10):
    # Генерация случайного графа
    graph = generate_random_graph(i, 0.8)
    # Поиск кратчайшего пути
    path, count = find_shortest_path(graph, list(graph.nodes())[0], list(graph.nodes())[-1])
    # Добавление количества операций в список
    operations_for_sizes.append(count)

# Построение графика зависимости количества операций от размерности графа
plt.plot(sizes, operations_for_sizes)
plt.xlabel('Размерность графа')
plt.ylabel('Количество операций')
plt.title('Сложность поиска кратчайшего пути в графе')
plt.show()
